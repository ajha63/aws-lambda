# aws-lambda

AWS-Lambda is a personal project that accumulates some lambda functions mostly developed with python.

Autho: Alvaro Hernández <hernandez.alvaro@gmail.com>

[![@ajha63](https://upload.wikimedia.org/wikipedia/commons/6/61/DevelopByAjha63.png)](https://gitlab.com/ajha63/)

[Alvaro Hernandez]: <by.ajha.work>
[@ajha63]: <https://twitter.com/ajha63>

